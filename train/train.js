const inputTrain = prompt('Enter train data: number, number of seats, max weight of luggage');
const numberOfPassengers = prompt('Enter number of passengers who want to register');
const inputTrainArr = inputTrain.split(' ');
let registeredPassengers = [];
let arrOfSeats = [];

console.log('Initial train data: ' + `number ${inputTrainArr[0]} , numberOfSeats ${inputTrainArr[1]}
, maxWeight ${inputTrainArr[2]}`);

const train = {
    number: +inputTrainArr[0],
    seats: new Array(+inputTrainArr[1]),
    luggage: {
        current: 0,
        max: +inputTrainArr[2]
    },
    register: function (passenger) {
        // passenger.seat - место, где хочет сидеть пассажир
        const seat = passenger.seat; // const { seat } = passenger;
        const id = passenger.id; // const { id } = passenger;
        const name = passenger.name; // const { id } = passenger;
        const luggage = passenger.luggage; // const { seat, id, luggage } = passenger;

        if (!this.seats[seat] && this.luggage.current + luggage <= this.luggage.max && seat < train.seats.length) { // если место свободно то есть там undefined, и багаж влазит
            this.seats[seat] = id; // регистрируем пассажира на рейс
            this.luggage.current += luggage; // кладем багаж пользователя в поезд
            registeredPassengers.push(`${seat} - ${name} ${id}`);
        }
    },
    // Функция должна вернуть кол-во свободных мест и массив номеров этих мест
    formSeatsData: function () {
        let counter = 0;
        for (let i = 0; i < this.seats.length; i++) {
            if (!this.seats[i]) {
                counter++;
                arrOfSeats.push(i);
            }
        }
        console.log(`NumberOfEmptySeats: ${counter}`);
        console.log('Array with numbers of empty seats');
        console.log(arrOfSeats);
    },
};

// Forming data about passengers
let passengers = [];
let newPersonData = {};

for (let i = 0; i < numberOfPassengers; i++) {
    let newPerson = prompt('Enter passenger data: id name seat luggage');
    let newPersonArr = newPerson.split(' ');
    newPersonData = {
        id: +newPersonArr[0],
        name: newPersonArr[1],
        seat: +newPersonArr[2],
        luggage: +newPersonArr[3]
    }
    passengers.push(newPersonData);
}
console.log('Initial data of passengers ');
console.log(passengers);

// Registration
for (let i = 0; i < numberOfPassengers; i++) {
    train.register(passengers[i]);
}

// Array of train seats with empty places - just for check
console.log('Registered places - array');
console.log(train.seats);

// Получение информации о списке пассажиров, прошедших регистрацию на рейс.
// Необходимо выдать массив строк, где каждая строка задается в следующем формате:
// "{номер_места} - {имя пассажира} {номер_паспорта}".
console.log('Array of registered passengers');
console.log(registeredPassengers);

//Получение информации о количестве свободных мест после регистрации всех пользователей.
// Функция должна вернуть кол-во свободных мест и массив номеров этих мест.
train.formSeatsData();

// Функция, которая по номеру паспорта пассажира выдаст место,
// на котором пассажир сидит. Если такого места нет, вернуть строку "No data".
let passportNumber = prompt('Enter passport number(id) of passenger to check his/her place in train');

function checkPassenger(passportNumber) {
    let passportNum;
    train.seats.forEach((elem, index) => {
        if (elem === +passportNumber) {
            passportNum = index;
        }
    })
    if (passportNum) {
        console.log(`The passenger's seat number is: ${passportNum}`);
    } else {
        console.log('No data');
    }
}

checkPassenger(passportNumber);

// Функция, которая выведет пассажиров, прошедщих регистрацию на поезд,
// в отсортированном порядке по убыванию веса их багажа.
// Формат вывода i-го пассажира: "{имя пассажира} - {вес багажа} кг"

function sortedPassengers() {
    let registeredPassengers = passengers.filter(passenger => train.seats.includes(passenger.id));
    registeredPassengers.sort((a, b) => a.luggage > b.luggage ? -1 : 1);
    registeredPassengers.forEach(elem => {
        console.log(`${elem.name} - ${elem.luggage} кг`);
    })
}

sortedPassengers();

